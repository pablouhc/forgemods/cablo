package space.pabuuu.cablo.player.render;

import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.GlStateManager.Profile;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderEntity;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.client.event.RenderLivingEvent.Post;
import net.minecraftforge.client.event.RenderLivingEvent.Pre;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;
import space.pabuuu.cablo.player.downloader.DownloadUpsideDown;

public class Dinnerbone {
  private static final Logger LOGGER = LogManager.getLogger();

  @SubscribeEvent
  public void doRenderPre(RenderPlayerEvent.Pre event) {
    EntityPlayer entity = event.getEntityPlayer(); // p_doRender_1
    RenderPlayer base = event.getRenderer();
    float partialRenderTick = event.getPartialRenderTick(); // p_doRender_9
    double x = event.getX(); // p_doRender_2
    double y = event.getY(); // p_doRender_4
    double z = event.getZ(); // p_doRender_6
    event.setCanceled(true);
    doRenderPlayer((AbstractClientPlayer) entity, x, y, z, partialRenderTick, base);
  }

  private void doRenderRenderer(EntityLivingBase p_doRender_1_, double p_doRender_2_, double p_doRender_4_, double p_doRender_6_, RenderLivingBase base) {
    if (!base.renderOutlines) {
      base.renderName(p_doRender_1_, p_doRender_2_, p_doRender_4_, p_doRender_6_);
    }

  }

  private void doRenderEntityLivingBase(EntityLivingBase p_doRender_1_, double p_doRender_2_, double p_doRender_4_, double p_doRender_6_, float p_doRender_9_, RenderLivingBase base) {
    GlStateManager.pushMatrix();
    GlStateManager.disableCull();
    base.mainModel.swingProgress = base.getSwingProgress(p_doRender_1_, p_doRender_9_);
    boolean shouldSit = p_doRender_1_.isRiding() && p_doRender_1_.getRidingEntity() != null && p_doRender_1_.getRidingEntity().shouldRiderSit();
    base.mainModel.isRiding = shouldSit;
    base.mainModel.isChild = p_doRender_1_.isChild();

    try {
      float f = base.interpolateRotation(p_doRender_1_.prevRenderYawOffset, p_doRender_1_.renderYawOffset, p_doRender_9_);
      float f1 = base.interpolateRotation(p_doRender_1_.prevRotationYawHead, p_doRender_1_.rotationYawHead, p_doRender_9_);
      float f2 = f1 - f;
      float f8;
      if (shouldSit && p_doRender_1_.getRidingEntity() instanceof EntityLivingBase) {
        EntityLivingBase entitylivingbase = (EntityLivingBase)p_doRender_1_.getRidingEntity();
        f = base.interpolateRotation(entitylivingbase.prevRenderYawOffset, entitylivingbase.renderYawOffset, p_doRender_9_);
        f2 = f1 - f;
        f8 = MathHelper.wrapDegrees(f2);
        if (f8 < -85.0F) {
          f8 = -85.0F;
        }

        if (f8 >= 85.0F) {
          f8 = 85.0F;
        }

        f = f1 - f8;
        if (f8 * f8 > 2500.0F) {
          f += f8 * 0.2F;
        }

        f2 = f1 - f;
      }

      float f7 = p_doRender_1_.prevRotationPitch + (p_doRender_1_.rotationPitch - p_doRender_1_.prevRotationPitch) * p_doRender_9_;
      base.renderLivingAt(p_doRender_1_, p_doRender_2_, p_doRender_4_, p_doRender_6_);
      f8 = base.handleRotationFloat(p_doRender_1_, p_doRender_9_);
      applyRotationsPlayer((AbstractClientPlayer)  p_doRender_1_, f8, f, p_doRender_9_, base);
      float f4 = base.prepareScale(p_doRender_1_, p_doRender_9_);
      float f5 = 0.0F;
      float f6 = 0.0F;
      if (!p_doRender_1_.isRiding()) {
        f5 = p_doRender_1_.prevLimbSwingAmount + (p_doRender_1_.limbSwingAmount - p_doRender_1_.prevLimbSwingAmount) * p_doRender_9_;
        f6 = p_doRender_1_.limbSwing - p_doRender_1_.limbSwingAmount * (1.0F - p_doRender_9_);
        if (p_doRender_1_.isChild()) {
          f6 *= 3.0F;
        }

        if (f5 > 1.0F) {
          f5 = 1.0F;
        }

        f2 = f1 - f;
      }

      GlStateManager.enableAlpha();
      base.mainModel.setLivingAnimations(p_doRender_1_, f6, f5, p_doRender_9_);
      base.mainModel.setRotationAngles(f6, f5, f8, f2, f7, f4, p_doRender_1_);
      boolean flag1;
      if (base.renderOutlines) {
        flag1 = base.setScoreTeamColor(p_doRender_1_);
        GlStateManager.enableColorMaterial();
        GlStateManager.enableOutlineMode(base.getTeamColor(p_doRender_1_));
        if (!base.renderMarker) {
          base.renderModel(p_doRender_1_, f6, f5, f8, f2, f7, f4);
        }

        if (!(p_doRender_1_ instanceof EntityPlayer) || !((EntityPlayer)p_doRender_1_).isSpectator()) {
          base.renderLayers(p_doRender_1_, f6, f5, p_doRender_9_, f8, f2, f7, f4);
        }

        GlStateManager.disableOutlineMode();
        GlStateManager.disableColorMaterial();
        if (flag1) {
          base.unsetScoreTeamColor();
        }
      } else {
        flag1 = base.setDoRenderBrightness(p_doRender_1_, p_doRender_9_);
        base.renderModel(p_doRender_1_, f6, f5, f8, f2, f7, f4);
        if (flag1) {
          base.unsetBrightness();
        }

        GlStateManager.depthMask(true);
        if (!(p_doRender_1_ instanceof EntityPlayer) || !((EntityPlayer)p_doRender_1_).isSpectator()) {
          base.renderLayers(p_doRender_1_, f6, f5, p_doRender_9_, f8, f2, f7, f4);
        }
      }

      GlStateManager.disableRescaleNormal();
    } catch (Exception var20) {
      LOGGER.error("Couldn't render entity", var20);
    }

    GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
    GlStateManager.enableTexture2D();
    GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
    GlStateManager.enableCull();
    GlStateManager.popMatrix();
    doRenderRenderer(p_doRender_1_, p_doRender_2_, p_doRender_4_, p_doRender_6_, base);
  }

  private void doRenderPlayer(AbstractClientPlayer p_doRender_1_, double p_doRender_2_, double p_doRender_4_, double p_doRender_6_, float p_doRender_9_, RenderPlayer base) {
    if (!p_doRender_1_.isUser() || base.getRenderManager().renderViewEntity == p_doRender_1_) {
      double d0 = p_doRender_4_;
      if (p_doRender_1_.isSneaking()) {
        d0 = p_doRender_4_ - 0.125D;
      }

      base.setModelVisibilities(p_doRender_1_);
      GlStateManager.enableBlendProfile(Profile.PLAYER_SKIN);
      doRenderEntityLivingBase(p_doRender_1_, p_doRender_2_, d0, p_doRender_6_, p_doRender_9_, base);
      GlStateManager.disableBlendProfile(Profile.PLAYER_SKIN);
    }
  }

  private void applyRotationsPlayer(AbstractClientPlayer p_applyRotations_1_, float p_applyRotations_2_, float p_applyRotations_3_, float p_applyRotations_4_, RenderLivingBase base) {
    if (p_applyRotations_1_.isEntityAlive() && p_applyRotations_1_.isPlayerSleeping()) {
      GlStateManager.rotate(p_applyRotations_1_.getBedOrientationInDegrees(), 0.0F, 1.0F, 0.0F);
      GlStateManager.rotate(base.getDeathMaxRotation(p_applyRotations_1_), 0.0F, 0.0F, 1.0F);
      GlStateManager.rotate(270.0F, 0.0F, 1.0F, 0.0F);
    } else if (p_applyRotations_1_.isElytraFlying()) {
      applyRotationsLivingBase(p_applyRotations_1_, p_applyRotations_2_, p_applyRotations_3_, p_applyRotations_4_, base);
      float f = (float)p_applyRotations_1_.getTicksElytraFlying() + p_applyRotations_4_;
      float f1 = MathHelper.clamp(f * f / 100.0F, 0.0F, 1.0F);
      GlStateManager.rotate(f1 * (-90.0F - p_applyRotations_1_.rotationPitch), 1.0F, 0.0F, 0.0F);
      Vec3d vec3d = p_applyRotations_1_.getLook(p_applyRotations_4_);
      double d0 = p_applyRotations_1_.motionX * p_applyRotations_1_.motionX + p_applyRotations_1_.motionZ * p_applyRotations_1_.motionZ;
      double d1 = vec3d.x * vec3d.x + vec3d.z * vec3d.z;
      if (d0 > 0.0D && d1 > 0.0D) {
        double d2 = (p_applyRotations_1_.motionX * vec3d.x + p_applyRotations_1_.motionZ * vec3d.z) / (Math.sqrt(d0) * Math.sqrt(d1));
        double d3 = p_applyRotations_1_.motionX * vec3d.z - p_applyRotations_1_.motionZ * vec3d.x;
        GlStateManager.rotate((float)(Math.signum(d3) * Math.acos(d2)) * 180.0F / 3.1415927F, 0.0F, 1.0F, 0.0F);
      }
    } else {
      applyRotationsLivingBase(p_applyRotations_1_, p_applyRotations_2_, p_applyRotations_3_, p_applyRotations_4_, base);
    }
  }
  private void applyRotationsLivingBase(EntityLivingBase p_applyRotations_1_, float p_applyRotations_2_, float p_applyRotations_3_, float p_applyRotations_4_, RenderLivingBase base) {
    GlStateManager.rotate(180.0F - p_applyRotations_3_, 0.0F, 1.0F, 0.0F);
    if (p_applyRotations_1_.deathTime > 0) {
      float f = ((float)p_applyRotations_1_.deathTime + p_applyRotations_4_ - 1.0F) / 20.0F * 1.6F;
      f = MathHelper.sqrt(f);
      if (f > 1.0F) {
        f = 1.0F;
      }

      GlStateManager.rotate(f * base.getDeathMaxRotation(p_applyRotations_1_), 0.0F, 0.0F, 1.0F);
    } else {
      String s = TextFormatting.getTextWithoutFormattingCodes(p_applyRotations_1_.getName());
      if (s != null && (DownloadUpsideDown.isUpsideDown(p_applyRotations_1_.getUniqueID().toString().replace("-", ""))) && (!(p_applyRotations_1_ instanceof EntityPlayer) || ((EntityPlayer)p_applyRotations_1_).isWearing(EnumPlayerModelParts.CAPE))) {
        GlStateManager.translate(0.0F, p_applyRotations_1_.height + 0.1F, 0.0F);
        GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
      }
    }
  }

}
