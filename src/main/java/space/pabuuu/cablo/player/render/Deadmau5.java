package space.pabuuu.cablo.player.render;

import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.util.ResourceLocation;
import space.pabuuu.cablo.player.downloader.DownloadEars;

public class Deadmau5 implements LayerRenderer<AbstractClientPlayer>
{
  private final RenderPlayer playerRenderer;

  public Deadmau5(RenderPlayer playerRendererIn)
  {
    this.playerRenderer = playerRendererIn;
  }

  public void doRenderLayer(AbstractClientPlayer entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale)
  {
    ResourceLocation rl = DownloadEars.getEarsResourceLocation(entitylivingbaseIn);
    if (!entitylivingbaseIn.isInvisible() && rl != null)
    {
      this.playerRenderer.bindTexture(rl);

      for (int i = 0; i < 2; i++)
      {
        float d = 0.0F;
        if (entitylivingbaseIn.isSneaking()) {
          d = 0.25F;
        }

        float f = entitylivingbaseIn.prevRotationYaw + (entitylivingbaseIn.rotationYaw - entitylivingbaseIn.prevRotationYaw) * partialTicks - (entitylivingbaseIn.prevRenderYawOffset + (entitylivingbaseIn.renderYawOffset - entitylivingbaseIn.prevRenderYawOffset) * partialTicks);
        float f1 = entitylivingbaseIn.prevRotationPitch + (entitylivingbaseIn.rotationPitch - entitylivingbaseIn.prevRotationPitch) * partialTicks;

        GlStateManager.pushMatrix();
        GlStateManager.rotate(f, 0.0F, 1.0F, 0.0F);
        GlStateManager.rotate(f1, 1.0F, 0.0F, 0.0F);
        GlStateManager.translate(0.375F * (i * 2 - 1), d, 0.0F);
        GlStateManager.translate(0.0F, -0.375F, 0.0F);
        GlStateManager.rotate(-f1, 1.0F, 0.0F, 0.0F);
        GlStateManager.rotate(-f, 0.0F, 1.0F, 0.0F);
        float f2 = 1.3333334F;
        GlStateManager.scale(f2, f2, f2);
        this.playerRenderer.getMainModel().renderDeadmau5Head(0.0625F);
        GlStateManager.popMatrix();
      }
    }
  }

// /\*[^\*]*\*/\s+

  public boolean shouldCombineTextures()
  {
    return true;
  }
}
