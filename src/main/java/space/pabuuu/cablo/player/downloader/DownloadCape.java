package space.pabuuu.cablo.player.downloader;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ImageBufferDownload;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IResource;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.Sys;
import space.pabuuu.cablo.init.Cablo;

public class DownloadCape {
  private static HashMap<String, Boolean> playersCape = new HashMap<>();
  private static HashMap<String, Boolean> playersElytra = new HashMap<>();
  public static void download(String uuid) {
    System.out.println("UUID:" + uuid);
    if ((uuid != null) && (!uuid.isEmpty())) {
      String url = Cablo.API_URL + "/capes/uuid/" + uuid;
      ResourceLocation rl = new ResourceLocation("capes/" + uuid);
      if (!resourceExists(rl)) {
        TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();
        ITextureObject tex = textureManager.getTexture(rl);

        IImageBuffer iib = new IImageBuffer() {
          ImageBufferDownload ibd = new ImageBufferDownload();

          public BufferedImage parseUserSkin(BufferedImage var1) {
            return DownloadCape.parseCape(var1, uuid);
          }


          public void skinAvailable() {}
        };
        ThreadDownloadImageData textureCape = new ThreadDownloadImageData(null, url, null, iib);
        textureManager.loadTexture(rl, textureCape);
      }
    }
  }

  public static BufferedImage parseCape(BufferedImage img, String uuid) {
    int imageWidth = 64;
    int imageHeight = 32;

    BufferedImage srcImg = img;
    int srcWidth = srcImg.getWidth();
    int srcHeight = srcImg.getHeight();
    while ((imageWidth < srcWidth) || (imageHeight < srcHeight)) {
      imageWidth *= 2;
      imageHeight *= 2;
    }

    BufferedImage imgNew = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
    Graphics g = imgNew.getGraphics();
    g.drawImage(img, 0, 0, (ImageObserver)null);
    g.dispose();

    playersCape.put(uuid, true);
    // Invisible elytra are not a good thing.
    playersElytra.put(uuid, !isElytraTexutreEmpty(imgNew));

//    System.out.println(imageWidth);
//    System.out.println(Arrays.toString(pixels));
//    byte[] pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
//    System.out.println(pixels);
//    isElytraTexutreEmpty(img);
    return imgNew;
  }

  private static Boolean hasElytra(String uuid) {
    Boolean hashMapResult = playersElytra.get(uuid);
    if (hashMapResult == null) return false;
    return hashMapResult;
  }

  private static Boolean hasCape(String uuid) {
    Boolean hashMapResult = playersCape.get(uuid);
    if (hashMapResult == null) return false;
    return hashMapResult;
  }

  private static boolean isElytraTexutreEmpty(BufferedImage img) {
    /*
       x=22, y=0 ______________ x=50, y=0
          |                        |
          |                        |
          |         29x26          |                 Elytra texture
          |                        |
          |                        |
       x=22, y=-25 ____________ x=50, y-25
    */

    int width = 29;
    int height = 26;
    BufferedImage newImage = img.getSubimage(22, 0, width, height);
    int[][] pixels2D = Cablo.bufferedImageTo2DArray(newImage);


//    System.out.println(Arrays.toString(pixels2D));

    // Presupongo que está vacía
    boolean empty = true;
    for (int[] x : pixels2D) {
      for (int y : x) {
//        System.out.println(y);
        if (y != 0 && y != 1) {
          // Este pixel es de un color que no es ni blanco ni transparente, por tanto hay algo en la textura de elytra
          // Y no está vacía
          empty = false;
          break;
        }
      }
      if (!empty) break;
    }

    return empty;
  }

  public static ResourceLocation getCapeResourceLocation(EntityLivingBase entitylivingbaseIn) {
    String playerUUID = entitylivingbaseIn.getUniqueID().toString().replace("-", "");
    ResourceLocation resourceLocation = new ResourceLocation("capes/" + playerUUID);
    return hasCape(playerUUID) ? resourceLocation : null;
  }

  public static ResourceLocation getElytraResourceLocation(EntityLivingBase entitylivingbaseIn) {
    String playerUUID = entitylivingbaseIn.getUniqueID().toString().replace("-", "");
    ResourceLocation resourceLocation = new ResourceLocation("capes/" + playerUUID);
    return hasElytra(playerUUID) ? resourceLocation : null;
  }

  public static boolean resourceExists(ResourceLocation resourceLocation) {
    try {
      IResource resource = Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation);
      if (resource != null)
        return true;
    } catch (IOException localIOException) {}
    return false;
  }
}
