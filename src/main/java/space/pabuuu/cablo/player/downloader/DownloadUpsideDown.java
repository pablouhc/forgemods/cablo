package space.pabuuu.cablo.player.downloader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IResource;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.IOUtils;
import space.pabuuu.cablo.init.Cablo;

public class DownloadUpsideDown {
  private static HashMap<String, Boolean> playersUpsideDown = new HashMap<>();

  public static Boolean isUpsideDown(String uuid) {
    Boolean hashMapResult = playersUpsideDown.get(uuid);
    if (hashMapResult == null) return false;
    return hashMapResult;
  }

  public static void download(String uuid) {
    System.out.println("UUID:"   + uuid);
    if ((uuid != null) && (!uuid.isEmpty())) {
      String url = Cablo.API_URL + "/upsidedown/uuid/" + uuid;
      Thread thread = new Thread(() -> {
        try {
          URL urlObject = new URL(url);
          URLConnection con = urlObject.openConnection();
          InputStream in = con.getInputStream();
          String encoding = con.getContentEncoding();
          encoding = encoding == null ? "UTF-8" : encoding;
          String body = IOUtils.toString(in, encoding);
          parseUpsideDown(body, uuid);
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
      thread.start();
    }
  }

  public static void parseUpsideDown(String body, String uuid) {
    if (body.equals("true")) {
      playersUpsideDown.put(uuid, true);
    } else if (body.equals("false")) {
      playersUpsideDown.put(uuid, false);
    }
  }
}
