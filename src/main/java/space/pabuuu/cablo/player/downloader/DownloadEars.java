package space.pabuuu.cablo.player.downloader;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.util.HashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ImageBufferDownload;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IResource;
/*    */
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import space.pabuuu.cablo.init.Cablo;


public class DownloadEars
{
  private static HashMap<String, Boolean> playersEars = new HashMap<>();

  private static Boolean hasEars(String uuid) {
    Boolean hashMapResult = playersEars.get(uuid);
    if (hashMapResult == null) return false;
    return hashMapResult;
  }

  public static void download(String uuid) {
    System.out.println("UUID:" + uuid);
    if ((uuid != null) && (!uuid.isEmpty())) {
      String url = Cablo.API_URL + "/ears/uuid/" + uuid;
      ResourceLocation rl = new ResourceLocation("ears/" + uuid);
      if (!resourceExists(rl)) {
        TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();
        ITextureObject tex = textureManager.getTexture(rl);

        IImageBuffer iib = new IImageBuffer() {
          ImageBufferDownload ibd = new ImageBufferDownload();

          public BufferedImage parseUserSkin(BufferedImage var1) {
            return DownloadEars.parseEars(var1, uuid);
          }

          public void skinAvailable() {}
        };
        ThreadDownloadImageData textureCape = new ThreadDownloadImageData(null, url, null, iib);
        textureManager.loadTexture(rl, textureCape);
      }
    }
  }

  public static BufferedImage parseEars(BufferedImage img, String uuid) {
    int imageWidth = 64;
    int imageHeight = 64;

    BufferedImage imgNew = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
    Graphics g = imgNew.getGraphics();
    g.drawImage(img, 24, 0, (ImageObserver)null);
    g.dispose();
    playersEars.put(uuid, true);
    return imgNew;
  }

  public static ResourceLocation getEarsResourceLocation(EntityLivingBase entitylivingbaseIn) {
    String playerUUID = entitylivingbaseIn.getUniqueID().toString().replace("-", "");
    ResourceLocation resourceLocation = new ResourceLocation("ears/" + playerUUID);
    return hasEars(playerUUID) ? resourceLocation : null;
  }

  public static boolean resourceExists(ResourceLocation resourceLocation) {
    try {
      IResource resource = Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation);
      if (resource != null)
        return true;
    } catch (IOException localIOException) {}
    return false;
  }
}
