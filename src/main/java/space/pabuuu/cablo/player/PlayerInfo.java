package space.pabuuu.cablo.player;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import space.pabuuu.cablo.player.downloader.DownloadCape;
import space.pabuuu.cablo.player.downloader.DownloadEars;
import space.pabuuu.cablo.player.downloader.DownloadUpsideDown;

public class PlayerInfo {
  private String nameClear = null;

  @SubscribeEvent
  public void PlayerJoin(EntityJoinWorldEvent event) {
    if ((event.getEntity() instanceof EntityPlayer)) {
      EntityPlayer player = (EntityPlayer) event.getEntity();
      DownloadCape.download(player.getUniqueID().toString().replace("-", ""));
      DownloadEars.download(player.getUniqueID().toString().replace("-", ""));
      DownloadUpsideDown.download(player.getUniqueID().toString().replace("-", ""));
    }
  }

  public String getNameClear() {
    return this.nameClear;
  }
}