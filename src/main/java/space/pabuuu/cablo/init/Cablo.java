package space.pabuuu.cablo.init;

import com.google.common.collect.Lists;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.ImageObserver;
import java.lang.reflect.Field;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import org.apache.logging.log4j.Logger;
import space.pabuuu.cablo.player.PlayerInfo;
import space.pabuuu.cablo.player.render.Deadmau5;
import space.pabuuu.cablo.player.render.Dinnerbone;
import space.pabuuu.cablo.player.render.LayerCape;
import space.pabuuu.cablo.player.render.LayerElytra;

@Mod(
    modid = Cablo.MODID,
    name = Cablo.NAME,
    version = Cablo.VERSION,
    clientSideOnly = true
)
public class Cablo
{
  public static final String MODID = "cablo";
  public static final String NAME = "Capes Pablo";
  public static final String VERSION = "1.0";

  private static Logger logger;

  public static String MOD_DIR;

  public static final String API_URL = "http://api.uhc.pabuuu.space";

  public static int[][] bufferedImageTo2DArray(BufferedImage imageIn) {
    BufferedImage image = new BufferedImage(imageIn.getWidth(), imageIn.getWidth(), BufferedImage.TYPE_INT_ARGB);
    Graphics g = image.getGraphics();
    g.drawImage(imageIn, 0, 0, (ImageObserver)null);
    g.dispose();
    int[][] pixels2D = new int[image.getWidth()][image.getHeight()]; // x, y
    int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
    for (int row = 0; row < image.getHeight(); row++) { // height = rows
      for (int column = 0; column < image.getWidth(); column++) { // width = columns
        pixels2D[column][row] = pixels[row * image.getWidth() + column];
      }
    }

    return pixels2D;
  }

  @EventHandler
  public void preInit(FMLPreInitializationEvent event)
  {
    logger = event.getModLog();
    MOD_DIR = event.getModConfigurationDirectory().getAbsolutePath();
  }

  @EventHandler
  public void init(FMLInitializationEvent event)
  {
    logger.info(API_URL);
    MinecraftForge.EVENT_BUS.register(new PlayerInfo());
//    Minecraft.getMinecraft().gameSettings.setModelPartEnabled(EnumPlayerModelParts.CAPE, true);


    for (RenderPlayer renderer : Minecraft.getMinecraft().getRenderManager().getSkinMap().values()) {
      removeMinecraftLayers(renderer);
      renderer.addLayer(new LayerCape(renderer));
      renderer.addLayer(new Deadmau5(renderer));
      renderer.addLayer(new LayerElytra(renderer));
    }
    MinecraftForge.EVENT_BUS.register(new Dinnerbone());

  }

  private void removeMinecraftLayers(RenderPlayer renderer) {
    renderer.layerRenderers.removeIf(net.minecraft.client.renderer.entity.layers.LayerElytra.class::isInstance);
    renderer.layerRenderers.removeIf(net.minecraft.client.renderer.entity.layers.LayerCape.class::isInstance);
  }

  private void removeMinecraftLayersReflection(RenderPlayer renderer) {
    Class renderClass = renderer.getClass();
    Field layerRenderersField = ReflectionHelper
        .findField(renderClass.getSuperclass(), "layerRenderers", "field_177097_h");
    layerRenderersField.setAccessible(true);

    List<LayerRenderer<EntityLivingBase>> layerRenderers = Lists.newArrayList();
    try {
      layerRenderers = (List<LayerRenderer<EntityLivingBase>>) layerRenderersField.get(renderer);
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }

    layerRenderers.removeIf(net.minecraft.client.renderer.entity.layers.LayerElytra.class::isInstance);
    layerRenderers.removeIf(net.minecraft.client.renderer.entity.layers.LayerCape.class::isInstance);

    try {
      layerRenderersField.set(renderer, layerRenderers);
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }
}
